function [A]  = NOKMeans(XX,bit,lambda1,Miter)
lambda=lambda1*size(XX,1);

[pc, l] = eigs(cov(double(XX)),bit);
X=XX;
V = XX * pc;
R = randn(bit,bit);
[U11 S2 V2] = svd(R);
A=pc*U11; %initial the projection
II=eye(size(A,1));
I1=eye(bit);
XPX=X'*X;%used to speed the calculation
for iter=0:Miter
    %optimize Q, B
    Z =X*A;      
    B = ones(size(Z,1),size(Z,2)).*-1;
    B(Z>=0) = 1;
    %optimize A (here Q is absorbed to A)
    XPB=X'*B;%used to speed the calculatin
    Grad=(XPX- lambda*II+lambda*(A*A'))*A-XPB;
    cost_max=norm(X*A-B,'fro').^2/2+lambda/4*norm(A'*A-I1,'fro').^2;
    for i=1:100
        A_temp=A-Grad;
        cost_line=norm(X*A_temp-B,'fro').^2/2+lambda/4*norm(A_temp'*A_temp-I1,'fro').^2;
        if cost_max > cost_line
            A=A_temp;
           break;
        end
        Grad=0.125*Grad;
    end
    AA_for_print=A'*A;
end


