NOKMeans. Version 1.1
Xiping Fu
fxpfxp0607@gmail.com
------------------------------------------------
demo.m shows an example of the proposed method. The trainNOKMeans.m is used for learning the hashing functions. The encodeNOKMeans.m is used for encoding the data points.

If you use this toolbox in your work, we would be grateful if you cite:
@INPROCEEDINGS{fu2014NOKmeans,
  author = {Fu, Xiping and McCane, Brendan and Mills, Steven and Albert, Michael},
  title = {NOKmeans: Non-orthogonal k-means hashing},
  booktitle = {Proceedings of the 12th Asian Conference on Computer Vision},
  year = {2014}
}