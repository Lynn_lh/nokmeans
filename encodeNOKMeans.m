function [B, U,UO] = encodeNOKMeans(X, NOKMeansparam)

mu=NOKMeansparam.mu;
X = bsxfun(@minus, X, mu);

pc = NOKMeansparam.A;
U = X*pc;
UO=U;
B = compactbit(U>0);
U = (U>0);



